﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using static System.Linq.Enumerable;
using ValueSetter = System.Action<dimov.Model, System.Collections.Generic.Dictionary<string, string>>;

namespace dimov {
    public class ObjectMapping {
        Dictionary<string, string>[] _data;

        private static List<ValueSetter> PropsForReflection = typeof(Model)
            .GetProperties(BindingFlags.Instance | BindingFlags.Public)
            .Select<PropertyInfo, ValueSetter>(prop => {
                var name = prop.GetCustomAttribute<PropertyAttribute>().Name;
                return (model, val) => {
                    var value = val[name];
                    prop.SetValue(model, prop.PropertyType switch {
                        var t when t == typeof(int) => int.Parse(value),
                        var t when t == typeof(double) => double.Parse(value),
                        var t when t == typeof(DateTime) => DateTime.Parse(value),
                        var t when t == typeof(float) => float.Parse(value),
                        var t when t == typeof(string) => value,
                        _ => null
                    });
                };
            }).ToList();

        private static ValueSetter SetterForAlexeev = CreateValueSetter();

        private static ValueSetter CreateValueSetter() {
            var props = typeof(Model)
                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Select(prop => (prop: prop, name: prop.GetCustomAttribute<PropertyAttribute>().Name))
                .ToList();

            var dictExpr = Expression.Parameter(typeof(Dictionary<string, string>), "val");
            var indexer = (dictExpr.Type.GetDefaultMembers().OfType<PropertyInfo>()
                .Where(p => p.PropertyType == typeof(string))
                .Select(p => new {p, q = p.GetIndexParameters()})
                .Where(t => t.q.Length == 1 && t.q[0].ParameterType == typeof(string))
                .Select(t => t.p)).Single();

            var args = new[] {typeof(string)};
            var parsers = new Dictionary<Type, MethodInfo> {
                [typeof(int)] = typeof(int).GetMethod(nameof(int.Parse), args),
                [typeof(double)] = typeof(double).GetMethod(nameof(double.Parse), args),
                [typeof(DateTime)] = typeof(DateTime).GetMethod(nameof(DateTime.Parse), args),
                [typeof(float)] = typeof(float).GetMethod(nameof(float.Parse), args),
            };

            var instance = Expression.Parameter(typeof(Model), "model");
            var expressions = new List<BinaryExpression>();
            foreach (var (prop, name) in props) {
                var keyExpr = Expression.Constant(name);
                Expression maybeParseExpr = Expression.Property(dictExpr, indexer, keyExpr);
                if (prop.PropertyType != typeof(string)) {
                    var parserMethod = parsers[prop.PropertyType];
                    maybeParseExpr = Expression.Call(parserMethod, maybeParseExpr);
                }

                var body = Expression.Assign(Expression.Property(instance, prop.Name), maybeParseExpr);
                expressions.Add(body);
            }

            var expression = Expression.Block(expressions);

            return Expression.Lambda<Action<Model, Dictionary<string, string>>>(expression, instance, dictExpr)
                .Compile();
        }

        [GlobalSetup]
        public void Setup() => _data = Range(0, 1000000).Select(i => new Dictionary<string, string> {
            ["x-Prop1"] = i.ToString(),
            ["x-Prop2"] = 1D.ToString(CultureInfo.InvariantCulture),
            ["x-Prop3"] = DateTime.Now.ToString(CultureInfo.InvariantCulture),
            ["x-Prop4"] = 1F.ToString(CultureInfo.InvariantCulture),
            ["x-Prop5"] = "1",
            ["x-Prop6"] = "2",
            ["x-Prop7"] = "3",
            ["x-Prop8"] = DateTime.Now.ToString(CultureInfo.InvariantCulture),
            ["x-Prop9"] = "1",
        }).ToArray();

        [Benchmark]
        public List<Model> DymovMethod() {
            var result = new List<Model>();

            foreach (var item in _data) {
                result.Add(new Model {
                    Prop1 = int.Parse(item["x-Prop1"]),
                    Prop2 = double.Parse(item["x-Prop2"]),
                    Prop3 = DateTime.Parse(item["x-Prop3"]),
                    Prop4 = float.Parse(item["x-Prop4"]),
                    Prop5 = item["x-Prop5"],
                    Prop6 = item["x-Prop6"],
                    Prop7 = item["x-Prop7"],
                    Prop8 = DateTime.Parse(item["x-Prop8"]),
                    Prop9 = item["x-Prop9"],
                });
            }

            return result;
        }

        [Benchmark]
        public List<Model> ReflectionMethod() {
            var result = new List<Model>();

            foreach (var item in _data) {
                var model = new Model();
                foreach (var prop in PropsForReflection) {
                    prop(model, item);
                }
            }

            return result;
        }

        [Benchmark]
        public List<Model> AlexeevMethod() {
            var result = new List<Model>();

            foreach (var item in _data) {
                var model = new Model();
                SetterForAlexeev(model, item);
                result.Add(model);
            }

            return result;
        }
    }

    public class Program {
        public static void Main() => BenchmarkRunner.Run<ObjectMapping>();
    }

    public class Model {
        [Property("x-Prop1")] public int Prop1 { get; set; }
        [Property("x-Prop2")] public double Prop2 { get; set; }
        [Property("x-Prop3")] public DateTime Prop3 { get; set; }
        [Property("x-Prop4")] public float Prop4 { get; set; }
        [Property("x-Prop5")] public string Prop5 { get; set; }
        [Property("x-Prop6")] public string Prop6 { get; set; }
        [Property("x-Prop7")] public string Prop7 { get; set; }
        [Property("x-Prop8")] public DateTime Prop8 { get; set; }
        [Property("x-Prop9")] public string Prop9 { get; set; }
    }

    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public sealed class PropertyAttribute : Attribute {
        public string Name { get; }
        public PropertyAttribute(string name) => Name = name;
    }
}
